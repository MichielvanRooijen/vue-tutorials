import firebase from 'firebase'
import 'firebase/firestore'

//firebase init

const config = {
    apiKey: "AIzaSyBKZIFBnJnalEykcRjm32hRGcUW2XSvrFQ",
    authDomain: "vuegram-bcaa6.firebaseapp.com",
    databaseURL: "https://vuegram-bcaa6.firebaseio.com",
    projectId: "vuegram-bcaa6",
    storageBucket: "",
    messagingSenderId: "447008050880" 
}
firebase.initializeApp(config)

//firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const currentUser = auth.currentUser

//data issue fix according to new firebase release
const settings = {
    timestampsInSnapshots: true
}
db.settings(settings)

//firebase collectors
const usersCollection = db.collection('users')
const postCollection = db.collection('posts')
const commentsCollection = db.collection('comments')
const likesCollection = db.collection('likes')

export {
    db,
    auth,
    currentUser,
    usersCollection,
    postCollection,
    commentsCollection,
    likesCollection
}